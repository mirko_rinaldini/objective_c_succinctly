//
//  main.m
//  ManualProperty
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        Person* person = [[Person alloc] init];
        [person setAge: 25];
        NSLog(@"Age is %u", [person age]);
        
    }
    return 0;
}

