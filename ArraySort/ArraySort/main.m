//
//  main.m
//  ArraySort
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger sortFunction(id item1, id item2, void *context) {
    float number1 = [item1 floatValue];
    float number2 = [item2 floatValue];
    if (number1 < number2) {
        return NSOrderedAscending;
    } else if (number1 > number2) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSNumber *n1 = [NSNumber numberWithFloat:22.5f];
        NSNumber *n2 = [NSNumber numberWithFloat:8.0f];
        NSNumber *n3 = [NSNumber numberWithFloat:-2.9f];
        NSNumber *n4 = [NSNumber numberWithFloat:13.1f];
        NSArray *numbers = [NSArray arrayWithObjects:n1, n2, n3, n4, nil];
        NSLog(@"%@", numbers);
        
        NSArray *sortedNumbers = [numbers
                                  sortedArrayUsingFunction:sortFunction
                                  context:NULL];
        NSLog(@"%@", sortedNumbers);
    }
    return 0;
}

