//
//  Person.m
//  Selectors
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
@synthesize name = _name;
@synthesize friend = _friend;
@synthesize action = _action;

- (void)sayHello {
    NSLog(@"Hello, says %@.", _name);
}

- (void)sayGoodbye {
    NSLog(@"Goodbye, says %@.", _name);
}

- (void)coerceFriend {
    NSLog(@"%@ is about to make %@ do something.", _name, [_friend name]);
    [_friend performSelector:_action];
}
@end
